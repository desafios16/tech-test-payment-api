using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {

        // Dados em memória.
        private static List<Venda> _vendas = new List<Venda>() {
            new Venda() {
                Id = 0,
                Status = StatusDoPedido.AguardandoPagamento,
                Vendedor = new Vendedor() {
                    Id = 0,
                    Nome = "José Carlos",
                    CPF = "12345678901",
                    Email = "josecarlos@hotmail.com",
                    Telefone = "(11)1234-1234"
                },
                Itens = new List<Item>()
                {
                    new Item() {
                        Id = 0,
                        Nome = "Ventilador Britânia",
                        Valor = 129.99M
                    }
                },
                Data = DateTime.Parse("2022-08-20T13:30:21.000-03:00")
            },
            new Venda() {
                Id = 1,
                Status = StatusDoPedido.EnviadoParaTransportadora,
                Vendedor = new Vendedor() {
                    Id = 1,
                    Nome = "Leonardo",
                    CPF = "11122233311",
                    Email = "leonardo@hotmail.com",
                    Telefone = "(11)3333-1234"
                },
                Itens = new List<Item>()
                {
                    new Item() {
                        Id = 1,
                        Nome = "Notebook Acer Aspire",
                        Valor = 2799.99M
                    },
                    new Item() {
                        Id = 2,
                        Nome = "HD SSD M2",
                        Valor = 299.99M
                    }
                },
                Data = DateTime.Parse("2022-08-13T15:11:21.000-03:00")
            },
            new Venda() {
                Id = 2,
                Status = StatusDoPedido.Entregue,
                Vendedor = new Vendedor() {
                    Id = 0,
                    Nome = "José Carlos",
                    CPF = "12345678901",
                    Email = "josecarlos@hotmail.com",
                    Telefone = "(11)1234-1234"
                },
                Itens = new List<Item>()
                {
                    new Item() {
                        Id = 3,
                        Nome = "Geladeira Brastemp",
                        Valor = 1899.99M
                    }
                },
                Data = DateTime.Parse("2022-06-13T15:11:21.000-03:00")
            }
        };

        [HttpGet("{id}")]
        public IActionResult ObterVendaPorId(int id)
        {
            var venda = _vendas.Find(v => v.Id == id);

            if (venda == null) return NotFound();
            return Ok(venda);
        }

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if (venda.Itens == null) return StatusCode(422, "A venda precisa ter no mínimo um item.");          
            
            venda.Id = _vendas.Select(v => v.Id).Max() + 1;
            _vendas.Add(venda);

            return CreatedAtAction(nameof(RegistrarVenda), new { id = venda.Id }, venda);
        }

        [HttpPut("AtualizarStatus/{id}")]
        public IActionResult AtualizarStatusDaVenda(int id, int status)
        {
            var vendaBanco = _vendas.Find(v => v.Id == id);
            if (vendaBanco == null) return NotFound();

            // De: `Aguardando pagamento` Para: `Pagamento Aprovado`
            // De: `Aguardando pagamento` Para: `Cancelada`
            if (status == 1 || status == 4)
            {
                if (vendaBanco.Status == StatusDoPedido.AguardandoPagamento)
                {
                    // 200 OK
                    return AlterarStatusDaVenda(vendaBanco, status);
                }
            }

            // De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
            // De: `Pagamento Aprovado` Para: `Cancelada`
            if (status == 2 || status == 4)
            {
                if (vendaBanco.Status == StatusDoPedido.PagamentoAprovado)
                {
                    // 200 OK
                    return AlterarStatusDaVenda(vendaBanco, status);
                }
            }

            // De: `Enviado para Transportador`. Para: `Entregue`
            if (status == 3)
            {
                if (vendaBanco.Status == StatusDoPedido.EnviadoParaTransportadora)
                {
                    // 200 OK
                    return AlterarStatusDaVenda(vendaBanco, status);
                }
            }

            // 304 Not Modified
            return StatusCode(304);
        }

        private IActionResult AlterarStatusDaVenda(Venda vendaBanco, int status)
        {
            int index = _vendas.IndexOf(vendaBanco);

            _vendas[index].Status = (StatusDoPedido)status;
            vendaBanco = _vendas[index];

            return Ok(vendaBanco);
        }


    }
}