using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public List<Item> Itens { get; set; }
        public DateTime Data { get; set; }
        public StatusDoPedido Status { get; set; }
        public Vendedor Vendedor { get; set; }
    }
}